
import 'package:rsqteka_frontend/bookshelf/model/BookIntegration.dart';
import 'package:rsqteka_frontend/bookshelf/model/data/Book.dart';
import 'package:rsqteka_frontend/utils/Result.dart';

class BookIntegrationMockerService extends BookIntegration {

  @override
  Future<Result<List<Book>>> getBooks() async {
    return Result.success([Book.mock(), Book.mock()]);
  }
}
