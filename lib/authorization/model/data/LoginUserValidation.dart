
import 'package:rsqteka_frontend/utils/ValidField.dart';

class LoginUserValidation {
  ValidField get loginValidation => _loginValidation;
  ValidField get passwordValidation => _passwordValidation;
  bool get incorrectCredentials => _incorrectCredentials;

  ValidField _loginValidation;
  ValidField _passwordValidation;
  bool _incorrectCredentials;

  LoginUserValidation.valid() {
    _loginValidation = ValidField.valid();
    _passwordValidation = ValidField.valid();
    _incorrectCredentials = false;
  }

  LoginUserValidation.incorrectCredentials() {
    _loginValidation = ValidField.valid();
    _passwordValidation = ValidField.valid();
    _incorrectCredentials = true;
  }

  LoginUserValidation(
      ValidField loginValidation,
      ValidField passwordValidation
  ) {
    this._loginValidation = loginValidation;
    this._passwordValidation = passwordValidation;
    _incorrectCredentials = false;
  }

}
