# RSQteka-frontend #

Welcome to RSQteka-frontend project. We're here to provide a better way of managing our neat company bookset.

### What is this repository for? ###

This repository is responsible for storing source code of RSQteka web application.

### How do I get set up? ###

TBA

### Contribution guidelines ###

You can contribute to this project simply by using the app and leave some feedback :)

### Who do I talk to? ###

Michał Najborowski & Jakub Kondarewicz
