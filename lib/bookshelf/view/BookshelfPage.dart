import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';
import 'package:rsqteka_frontend/authorization/model/LogoutUserModel.dart';
import 'package:rsqteka_frontend/bookshelf/model/BookshelfModel.dart';
import 'package:rsqteka_frontend/bookshelf/model/data/Book.dart';
import 'package:rsqteka_frontend/bookshelf/model/data/BookDetails.dart';
import 'package:rsqteka_frontend/bookshelf/model/data/BooksRetrieving.dart';
import 'package:rsqteka_frontend/components/Split.dart';

class BookshelfPage extends StatelessWidget {

  Widget build(BuildContext context) {
    Provider.of<BookshelfModel>(context, listen: false).getBooks();
    return CupertinoPageScaffold(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Container(
          child: Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Column(
              children: [
                Row(
                  children: [
                    Text("RSQteka",
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    Padding(
                      padding: const EdgeInsets.only(right: 20),
                      child: Icon(
                        Icons.local_library_sharp,
                        color: Colors.grey,
                        size: 25,
                      ),
                    ),
                    CupertinoButton(child: Text("Katalog"), onPressed: () {}),
                    CupertinoButton(
                        child: Text("Mój profil"), onPressed: () {}),
                    Spacer(),
                    Consumer<LogoutUserModel>(
                      builder: (c, x, s) {
                        return CupertinoButton(child: Text("Wyloguj"), onPressed: () {
                          Provider.of<LogoutUserModel>(context, listen: false).logoutUser();
                        });
                      },
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Consumer<BookshelfModel>(
                    builder: (context, model, child) {
                      return CupertinoTextField(
                        style: TextStyle(color: Colors.black),
                        prefix: Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Icon(Icons.search, color: Colors.grey),
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                        ),
                        onChanged: (value) {
                          Provider.of<BookshelfModel>(context, listen: false).searchBook(value);
                        },
                      );
                    },
                  ),
                ),
                Expanded(
                    child: Split(
                      padding: 40,
                      weightFactor: 0.6,
                      view1: SingleChildScrollView(
                        child: Column(
                          children: [
                            Consumer<BookshelfModel>(
                                builder: (context, model, child) {
                                  if(model.bookRetrieving.state != BookRetrievingState.RETRIEVED) {
                                    return Container();
                                  }
                                  var books = model.bookRetrieving.books;
                                  var filteredBooks = ((value) =>
                                  value == null ? books : books.where((
                                      element) =>
                                      element.title.toLowerCase().contains(value.toLowerCase())))(
                                      model.searchValue).toList();
                                  return ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: filteredBooks.length,
                                      itemBuilder: (context, index) {
                                        return Card(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                BorderRadius.circular(10)),
                                            child: Padding(
                                              padding: const EdgeInsets.all(
                                                  8.0),
                                              child: ListTile(
                                                leading: Container(
                                                  child: Icon(Icons.book,
                                                      color: Colors.grey),
                                                  height: double.infinity,
                                                ),
                                                title: Text(
                                                    filteredBooks[index].title,
                                                    style: TextStyle(
                                                        fontWeight: FontWeight
                                                            .bold,
                                                        fontSize: 18),
                                                    maxLines: 1,
                                                    overflow: TextOverflow
                                                        .ellipsis),
                                                subtitle: Column(
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceAround,
                                                  children: [
                                                    Padding(
                                                      padding:
                                                      const EdgeInsets
                                                          .symmetric(
                                                          vertical: 4),
                                                      child: Text(
                                                        filteredBooks[index].author,
                                                        maxLines: 1,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding:
                                                      const EdgeInsets
                                                          .symmetric(
                                                          vertical: 4),
                                                      child: Text(
                                                        "PSYCHOLOGIA",
                                                        maxLines: 1,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                trailing: Container(
                                                  height: double.infinity,
                                                  child: () {
                                                    switch (filteredBooks[index]
                                                        .status) {
                                                      case BookStatus
                                                          .AVAILABLE:
                                                        {
                                                          return Icon(
                                                              Icons.circle,
                                                              color: Colors
                                                                  .green);
                                                        }
                                                      case BookStatus
                                                          .BORROWED:
                                                        {
                                                          return Icon(
                                                              Icons.circle,
                                                              color: Colors
                                                                  .red);
                                                        }
                                                      case BookStatus.EBOOK:
                                                        {
                                                          return Icon(
                                                              Icons.circle,
                                                              color: Colors
                                                                  .blueAccent);
                                                        }
                                                    }
                                                  }(),
                                                ),
                                                onTap: () {
                                                  Provider.of<BookshelfModel>(
                                                      context,
                                                      listen: false)
                                                      .checkBook(BookDetails(
                                                      book: filteredBooks[index],
                                                      category:
                                                      "PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA PSYCHOLOGIA ",
                                                      description:
                                                      "ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ELO ",
                                                      isbn:
                                                      "1234-123134325-1234512453-1341",
                                                      language: "Polski",
                                                      publishingHouse: "Helios",
                                                      releaseYear: "1997"));
                                                },
                                              ),
                                            ));
                                      });
                                }
                            )
                          ],
                        ),
                      ),
                      view2: Consumer<BookshelfModel>(
                        builder: (context, model, child) {
                          if (model.bookDetails != null) {
                            return Padding(
                              padding:
                              const EdgeInsets.symmetric(horizontal: 8.0),
                              child: Material(
                                elevation: 1,
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    children: [
                                      Row(
                                        children: [
                                          Icon(Icons.book,
                                              color: Colors.grey, size: 100),
                                          Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            children: [
                                              Text(model.bookDetails.book
                                                  .title,
                                                  style: TextStyle(
                                                      fontWeight: FontWeight
                                                          .bold,
                                                      fontSize: 24),
                                                  maxLines: 2,
                                                  overflow:
                                                  TextOverflow.ellipsis),
                                              Padding(
                                                padding:
                                                const EdgeInsets.symmetric(
                                                    vertical: 4),
                                                child: Text(
                                                  model.bookDetails.book
                                                      .author,
                                                  style: TextStyle(
                                                      fontSize: 14),
                                                  maxLines: 1,
                                                  overflow: TextOverflow
                                                      .ellipsis,
                                                ),
                                              ),
                                              Padding(
                                                padding:
                                                const EdgeInsets.symmetric(
                                                    vertical: 10),
                                                child: CupertinoButton(
                                                  borderRadius: BorderRadius
                                                      .all(
                                                      Radius.circular(20)),
                                                  color: Colors.green,
                                                  child: Text("wypożycz"),
                                                  onPressed: () {},
                                                ),
                                              ),
                                              Row(
                                                children: [
                                                  Padding(
                                                    padding:
                                                    const EdgeInsets.only(
                                                        right: 8.0),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment
                                                          .start,
                                                      children: [
                                                        Padding(
                                                          padding:
                                                          const EdgeInsets
                                                              .all(8.0),
                                                          child: Text(
                                                            "Wydawnictwo:",
                                                            style: TextStyle(
                                                                fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                            maxLines: 1,
                                                            overflow: TextOverflow
                                                                .ellipsis,
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                          const EdgeInsets
                                                              .all(8.0),
                                                          child: Text(
                                                            "Język wydania:",
                                                            style: TextStyle(
                                                                fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                            maxLines: 1,
                                                            overflow: TextOverflow
                                                                .ellipsis,
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                          const EdgeInsets
                                                              .all(8.0),
                                                          child: Text(
                                                            "Rok wydania:",
                                                            style: TextStyle(
                                                                fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                            maxLines: 1,
                                                            overflow: TextOverflow
                                                                .ellipsis,
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                          const EdgeInsets
                                                              .all(8.0),
                                                          child: Text(
                                                            "ISBN:",
                                                            style: TextStyle(
                                                                fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                            maxLines: 1,
                                                            overflow: TextOverflow
                                                                .ellipsis,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Column(
                                                    crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                    children: [
                                                      Padding(
                                                        padding:
                                                        const EdgeInsets.all(
                                                            8.0),
                                                        child: Text(model
                                                            .bookDetails
                                                            .publishingHouse),
                                                      ),
                                                      Padding(
                                                        padding:
                                                        const EdgeInsets.all(
                                                            8.0),
                                                        child: Text(model
                                                            .bookDetails
                                                            .language),
                                                      ),
                                                      Padding(
                                                        padding:
                                                        const EdgeInsets.all(
                                                            8.0),
                                                        child: Text(model
                                                            .bookDetails
                                                            .releaseYear),
                                                      ),
                                                      Padding(
                                                        padding:
                                                        const EdgeInsets.all(
                                                            8.0),
                                                        child: Text(model
                                                            .bookDetails
                                                            .isbn),
                                                      )
                                                    ],
                                                  )
                                                ],
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text("Kategoria:",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold)),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          model.bookDetails.category,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text("Opis:",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold)),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          model.bookDetails.description,
                                          maxLines: 6,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text("Egzemplarze:",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold)),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          } else {
                            return Container();
                          }
                        },
                      ),
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
