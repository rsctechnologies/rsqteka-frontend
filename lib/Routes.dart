
class Routes {

  static final String SERVER_URI_ROOT = "http://10.1.11.176:9080";
  static final String BOOKS_URI = "http://localhost:8080";

  static final String BOOKSHELF = "/bookshelf";
  static final String STARTER = "/";
  static final String LOGIN = "/login";

}
