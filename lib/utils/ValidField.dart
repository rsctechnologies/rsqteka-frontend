
class ValidField {
  
  bool get valid => _valid;
  String get errorMessage => _errorMessage;

  bool _valid;
  String _errorMessage = "";
  
  ValidField.valid() {
    _valid = true;
    _errorMessage = "";
  }

  ValidField.invalid(String errorMessage) {
    _valid = false;
    _errorMessage = errorMessage;
  }
  
}
