
import 'package:universal_html/html.dart';

List<String> _keys = new List.empty(growable: true);

class Storage {

  static String get(String key) {
    return window.localStorage[key];
  }

  static void store(String key, String value) {
    _keys.add(key);
    window.localStorage[key] = value;
  }

  static void remove(String key) {
    _keys.removeWhere((element) => element == key);
    window.localStorage.remove(key);
  }

  static void removeAll() {
    _keys.forEach((element) {window.localStorage.remove(element);});
    _keys.removeWhere((element) => true);
  }

}