

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rsqteka_frontend/utils/Storage.dart';

import '../../Routes.dart';

class StarterPage extends StatefulWidget {
  @override
  _StarterPageState createState() => _StarterPageState();
}

class _StarterPageState extends State<StarterPage> {

  @override
  void initState() {
    Future.delayed(Duration.zero, () async {
      var t = Storage.get("refreshTokenExpiration");
      if(t != null) {
        DateTime refreshTokenExpiration =
            DateTime.parse(Storage.get("refreshTokenExpiration"));
        if(refreshTokenExpiration.isAfter(DateTime.now())) {
          Navigator.pushReplacementNamed(context, Routes.BOOKSHELF);
        } else {
          Navigator.pushReplacementNamed(context, Routes.LOGIN);
        }
      } else {
        Navigator.pushReplacementNamed(context, Routes.LOGIN);
      }

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Text("Loading..."),
    );
  }

}

