import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rsqteka_frontend/authorization/model/LogoutUserModel.dart';
import 'package:rsqteka_frontend/authorization/view/LoginPage.dart';
import 'package:rsqteka_frontend/bookshelf/model/BookshelfModel.dart';
import 'package:rsqteka_frontend/starter/view/StarterPage.dart';
import 'package:rsqteka_frontend/bookshelf/view/BookshelfPage.dart';

import 'Routes.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (create) => BookshelfModel()),
        ChangeNotifierProvider(create: (create) => LogoutUserModel(create))

      ],
      child: CupertinoApp(
        title: 'Flutter Demo',
        initialRoute: Routes.STARTER,
        routes: {
          Routes.STARTER: (context) => StarterPage(),
          Routes.LOGIN: (context) => LoginPage(),
          Routes.BOOKSHELF: (context) => BookshelfPage()
        },
      ),
    );
  }
}