
class LoginResponse {

  String get accessToken => _accessToken;
  String get refreshToken => _refreshToken;
  int get accessTokenExpiration => _accessTokenExpiration;
  int get refreshTokenExpiration => _refreshTokenExpiration;

  String _accessToken;
  String _refreshToken;
  int _accessTokenExpiration;
  int _refreshTokenExpiration;

  LoginResponse(this._accessToken, this._refreshToken, this._accessTokenExpiration, this._refreshTokenExpiration);

}
