
import 'dart:convert';

import 'package:rsqteka_frontend/Routes.dart';
import 'package:rsqteka_frontend/authorization/model/LoginIntegration.dart';
import 'package:rsqteka_frontend/authorization/model/data/LoginRequest.dart';
import 'package:rsqteka_frontend/utils/Result.dart';

import 'package:http/http.dart' as http;
import 'dart:convert' as convert ;
import 'data/LoginResponse.dart';


class LoginIntegrationRealService extends LoginIntegration {

  final String _loginUrl = Routes.SERVER_URI_ROOT + "/auth/realms/master/protocol/openid-connect/token";

  @override
  Future<Result<LoginResponse>> login(LoginRequest loginRequest) async {
    Map<String, dynamic> credentials = {
      "client_id": "rsqteka",
      "client_secret": "5ee84bc8-027e-4fdc-80b4-72b3259ee534",
      "grant_type": "password",
      "username": loginRequest.login,
      "password": loginRequest.password
    };
    print("SFADSGFADGDSG");
    http.Response response = await http.post(
        Uri.parse(_loginUrl),
        headers: {
          "Accept": "application/json",
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: credentials);
    print(response.statusCode);
    if(response.statusCode == 200) {
      var jsonResponse = jsonDecode(response.body);
      return Result.success(
        LoginResponse(
          jsonResponse["access_token"],
          jsonResponse["refresh_token"],
          jsonResponse["expires_in"],
          jsonResponse["refresh_expires_in"]
        )
      );
    }
    return Result.failed("");
  }

}
