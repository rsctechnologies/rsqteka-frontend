
import 'package:rsqteka_frontend/authorization/model/data/LoginRequest.dart';
import 'package:rsqteka_frontend/authorization/model/data/LoginResponse.dart';
import 'package:rsqteka_frontend/utils/Result.dart';

abstract class LoginIntegration {

  Future<Result<LoginResponse>> login(LoginRequest loginRequest);

}
