
import 'package:rsqteka_frontend/authorization/model/LoginIntegration.dart';
import 'package:rsqteka_frontend/authorization/model/data/LoginRequest.dart';
import 'package:rsqteka_frontend/utils/Result.dart';

import 'data/LoginResponse.dart';

class LoginIntegrationMockedService extends LoginIntegration {
  @override
  Future<Result<LoginResponse>> login(LoginRequest loginRequest) async {
    if(loginRequest.login == "login") {
      return Result.success(LoginResponse("access_token", "refresh_token", 300, 9000));
    }
    return Result.failed("");
  }

}
