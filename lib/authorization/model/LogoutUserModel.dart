
import 'package:flutter/cupertino.dart';
import 'package:rsqteka_frontend/Routes.dart';
import 'package:rsqteka_frontend/utils/Storage.dart';

class LogoutUserModel extends ChangeNotifier {

  BuildContext _context;

  LogoutUserModel(this._context);

  void logoutUser() {
    Storage.removeAll();
    Navigator.pushReplacementNamed(_context, Routes.LOGIN);
  }

}
