
import 'package:flutter/cupertino.dart';
import 'package:rsqteka_frontend/IntegrationProvider.dart';
import 'package:rsqteka_frontend/authorization/model/LoginIntegration.dart';
import 'package:rsqteka_frontend/authorization/model/data/LoginRequest.dart';
import 'package:rsqteka_frontend/authorization/model/data/LoginUserValidation.dart';
import 'package:rsqteka_frontend/utils/Storage.dart';
import 'package:rsqteka_frontend/utils/ValidField.dart';

import '../../Routes.dart';

class LoginUserModel extends ChangeNotifier {

  BuildContext _context;

  LoginUserModel(this._context);

  LoginUserValidation get loginUserValidation => _loginUserValidation;

  LoginUserValidation _loginUserValidation = LoginUserValidation.valid();

  void login(String login, String password) async {
    ValidField loginValid = login.length > 0 ? ValidField.valid() : ValidField.invalid("Please fill login");
    ValidField passwordValid = password.length > 4 ? ValidField.valid() : ValidField.invalid("Password is always longer than 4 letters");
    _loginUserValidation = LoginUserValidation(loginValid, passwordValid);
    if(!loginValid.valid || !passwordValid.valid) {
      notifyListeners();
    } else {
      LoginIntegration loginIntegration = IntegrationProvider.loginIntegration();
      var loginResult = await loginIntegration.login(LoginRequest(login, password));
      if(loginResult.succeed) {
        Storage.store("accessToken", loginResult.data.accessToken);
        Storage.store("refreshToken", loginResult.data.refreshToken);
        Storage.store("accessTokenExpiration", DateTime.now().add(Duration(seconds: loginResult.data.accessTokenExpiration)).toIso8601String());
        Storage.store("refreshTokenExpiration", DateTime.now().add(Duration(seconds: loginResult.data.refreshTokenExpiration)).toIso8601String());
        Future.delayed(Duration.zero, () {
          Navigator.pushReplacementNamed(_context, Routes.BOOKSHELF);
        });
      } else {
        _loginUserValidation = LoginUserValidation.incorrectCredentials();
        notifyListeners();
      }
    }
  }

}
