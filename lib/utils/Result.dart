
class Result<T> {

  bool get succeed => _succeed;
  T get data => _data;
  String get errorMessage => _errorMessage;

  bool _succeed;
  T _data;
  String _errorMessage;
  
  Result.success(T data) {
    _succeed = true;
    _data = data;
    _errorMessage = null;
  }
  
  Result.failed(String message) {
    _succeed = false;
    _data = null;
    _errorMessage = message;
  }
  
}
