import 'package:flutter/cupertino.dart';
import 'package:rsqteka_frontend/IntegrationProvider.dart';
import 'package:rsqteka_frontend/bookshelf/model/BookIntegration.dart';
import 'package:rsqteka_frontend/bookshelf/model/data/Book.dart';
import 'package:rsqteka_frontend/bookshelf/model/data/BooksRetrieving.dart';

import 'data/BookDetails.dart';

class BookshelfModel extends ChangeNotifier {
  BookRetrieving get bookRetrieving => _bookRetrieving;

  BookDetails _bookDetails;
  BookRetrieving _bookRetrieving = BookRetrieving.initial();

  BookDetails bookDetails;
  String searchValue;

  void getBooks() async {
    BookIntegration bookIntegration = IntegrationProvider.bookIntegration();
    var results = await bookIntegration.getBooks();
    if(results.succeed) {
      _bookRetrieving = BookRetrieving.succeed(results.data);
    } else {
      _bookRetrieving = BookRetrieving.errors();
      Future.delayed(Duration(seconds: 5), () {
          getBooks();
      });
    }
  }


  void checkBook(BookDetails newBookDetails) {
      bookDetails = newBookDetails;
      notifyListeners();
  }

  void searchBook(String newSearchValue) {
    searchValue = newSearchValue;
    notifyListeners();
  }
}