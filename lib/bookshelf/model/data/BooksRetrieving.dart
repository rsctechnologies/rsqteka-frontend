
import 'package:rsqteka_frontend/bookshelf/model/data/Book.dart';

enum BookRetrievingState {
  RETRIEVING,
  RETRIEVED,
  ERRORS
}

class BookRetrieving {

  BookRetrievingState get state => _state;
  List<Book> get books => _books;

  BookRetrievingState _state;
  List<Book> _books;

  BookRetrieving.succeed(List<Book> books) {
    _state = BookRetrievingState.RETRIEVED;
    _books = books;
  }

  BookRetrieving.initial() {
    _state = BookRetrievingState.RETRIEVING;
    _books = List.empty();
  }

  BookRetrieving.errors() {
    _state = BookRetrievingState.ERRORS;
    _books = List.empty();
  }

}
