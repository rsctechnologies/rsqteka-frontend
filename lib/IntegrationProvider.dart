
import 'package:rsqteka_frontend/authorization/model/LoginIntegration.dart';
import 'package:rsqteka_frontend/authorization/model/LoginIntegrationMockedService.dart';
import 'package:rsqteka_frontend/authorization/model/LoginIntegrationRealService.dart';
import 'package:rsqteka_frontend/bookshelf/model/BookIntegration.dart';
import 'package:rsqteka_frontend/bookshelf/model/BookIntegrationMockedService.dart';
import 'package:rsqteka_frontend/bookshelf/model/BookIntegrationRealService.dart';

class IntegrationProvider {

  static LoginIntegration loginIntegration() {
    return LoginIntegrationMockedService();
  }

  static BookIntegration bookIntegration() {
    return BookIntegrationRealService();
  }

}
