class Book {
  String id;
  String title;
  String author;
  String image;
  BookStatus status;

  Book({
    this.id,
    this.title,
    this.author,
    this.image,
    this.status
  });

  Book.fromJson(Map<String, dynamic> json) {
      id = json["id"];
      title = json["bookEntity"]["title"];
      status = BookStatus.AVAILABLE;
      image = "";
      author = (json["bookEntity"]["authors"] as List<dynamic>).join(",");
  }

  static Book mock() {
    return Book(id: "1", title: "Elo", author: "Mordo", image: "Obrazek", status: BookStatus.AVAILABLE);
  }

  static Book mock2() {
    return Book(id: "2", title: "Melo", author: "Pienta", image: "Obrazek", status: BookStatus.AVAILABLE);
  }

  static Book mock3() {
    return Book(id: "3", title: "Kordian", author: "Pienta", image: "Obrazek", status: BookStatus.AVAILABLE);
  }
}

enum BookStatus {
  AVAILABLE,
  BORROWED,
  EBOOK
}