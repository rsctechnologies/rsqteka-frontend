
import 'package:flutter/cupertino.dart';

class Split extends StatelessWidget {

  double weightFactor;
  double padding;
  Widget view1;
  Widget view2;

  Split({this.view1, this.view2, this.padding = 0, this.weightFactor = 0.5});

  @override
  Widget build(BuildContext context) {
    var mq = MediaQuery.of(context);
    var size = mq.size.width - padding;
    double viewWidth1 = size * weightFactor;
    double viewWidth2 = size * (1.0 - weightFactor);
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: viewWidth1,
          child: view1,
        ),
        Container(
          width: viewWidth2,
          child: view2,
        )
      ],
    );
  }

}

