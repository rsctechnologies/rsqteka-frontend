import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rsqteka_frontend/authorization/model/LoginUserModel.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String _login = "";
  String _password = "";

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => LoginUserModel(context))
      ],
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Container(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text("RSQteka",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        Padding(
                          padding: const EdgeInsets.only(right: 20),
                          child: Icon(
                            Icons.local_library_sharp,
                            color: Colors.grey,
                            size: 25,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Flexible(
                          child: Column(
                            textDirection: TextDirection.rtl,
                            children: [
                              Consumer<LoginUserModel>(
                                builder: (context, loginUserModel, child) {
                                  return ListView(
                                    shrinkWrap: true,
                                    children: [
                                      Text(loginUserModel.loginUserValidation
                                          .loginValidation.errorMessage),
                                      CupertinoTextField(
                                        onChanged: (login) {
                                          setState(() {
                                            _login = login;
                                          });
                                        },
                                      ),
                                      Text(loginUserModel.loginUserValidation
                                          .passwordValidation.errorMessage),
                                      CupertinoTextField(
                                        onChanged: (password) {
                                          setState(() {
                                            _password = password;
                                          });
                                        },
                                        obscureText: true,
                                      ),
                                      (loginUserModel.loginUserValidation
                                              .incorrectCredentials)
                                          ? Text("Incorrect username or password")
                                          : Text(""),
                                      CupertinoButton(
                                          child: Text("Zaloguj się"),
                                          onPressed: () {
                                            Provider.of<LoginUserModel>(context,
                                                    listen: false)
                                                .login(_login, _password);
                                          })
                                    ],
                                  );
                                },
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
