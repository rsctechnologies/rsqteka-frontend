import 'dart:convert';

import 'package:rsqteka_frontend/Routes.dart';
import 'package:rsqteka_frontend/bookshelf/model/BookIntegration.dart';
import 'package:rsqteka_frontend/bookshelf/model/data/Book.dart';
import 'package:rsqteka_frontend/utils/Result.dart';

import 'package:http/http.dart' as http;

class BookIntegrationRealService extends BookIntegration {
  @override
  Future<Result<List<Book>>> getBooks() async {
    var url = Uri.parse(Routes.BOOKS_URI + "/books");
    var response = await http.get(url,
        headers: {
      "Content-Type": "application/json",
        });
    if(response.statusCode == 200) {
      print(jsonDecode(response.body));
      List<dynamic> booksJson = jsonDecode(response.body);
      return Result.success(booksJson.map((e) => Book.fromJson(e)).toList());
    }
    return Result.failed("Something goes wrong");
  }
}
