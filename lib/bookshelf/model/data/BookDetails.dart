import 'Book.dart';

class BookDetails {
  Book book;
  String publishingHouse;
  String language;
  String releaseYear;
  String isbn;
  String category;
  String description;

  BookDetails(
      {this.book,
      this.publishingHouse,
      this.language,
      this.releaseYear,
      this.isbn,
      this.category,
      this.description});
}
