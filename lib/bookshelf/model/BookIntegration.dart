
import 'package:rsqteka_frontend/bookshelf/model/data/Book.dart';
import 'package:rsqteka_frontend/utils/Result.dart';

abstract class BookIntegration {

  Future<Result<List<Book>>> getBooks();

}